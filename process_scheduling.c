#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INPUT_FILE "plist.dat"

typedef struct Process{
    int PId;		//ÇÁ·Î¼¼½º ID
    int ArrTime;	//Arrive time
    int BurTime;	//Burst time
}Process;

typedef Process element; //¿ä¼ÒÀÇ Å¸ÀÔ

typedef struct QueueNode{ // Å¥ÀÇ ³ëµåÀÇ Å¸ÀÔ
    element item;
    struct QueueNode *link;
}QueueNode;
typedef struct{ //Å¥ ADT±¸Çö
    QueueNode *front,*rear;
}QueueType;



//¿À·ù ÇÔ¼ö
void error(char *message){
    fprintf(stderr,"%s\n",message);
    exit(1);
}
//ÃÊ±âÈ­ ÇÔ¼ö
void init(QueueType *q){
    q->front = q->rear =NULL;
}

//°ø¹é »óÅÂ °ËÃâ ÇÔ¼ö
int is_empty(QueueType *q){
    return (q->front==NULL);
}

//Æ÷È­ »óÅÂ °ËÃâ ÇÔ¼ö
int is_full(QueueType *q){
    return 0;
}

//»ðÀÔ ÇÔ¼ö
void enqueue(QueueType *q,element item){
    QueueNode *temp=(QueueNode*)malloc(sizeof(QueueNode));
    if(temp==NULL){
        error("¸Þ¸ð¸®¸¦ ÇÒ´çÇÒ ¼ö ¾ø½À´Ï´Ù.");
    }else{
        temp->item	= item;			//µ¥ÀÌÅÍ ÀúÀå
        temp->link	= NULL;			//¸µÅ© ÇÊµå¸¦ NULL
        if(is_empty(q)){			//Å¥°¡ °ø¹éÀÌ¸é
            q->front	= temp;
            q->rear		= temp;
        }else{						//Å¥°¡ °ø¹éÀÌ ¾Æ´Ï¸é
            q->rear->link	= temp; //¼ø¼­°¡ Áß¿ä
            q->rear			= temp;
        }
    }
}

//  ´Ü¼øÈ÷ Å¥ÀÇ µÚÂÊ¿¡ »ðÀÔÇÏ´Â °ÍÀÌ ¾Æ´Ï¶ó
//  Burst TimeÀÌ ÀÛÀº ÇÁ·Î¼¼½º°¡ ¾ÕÂÊ¿¡ ¿Àµµ·Ï Á¤·ÄÇÏ¿© ³Ö´Â ÇÔ¼ö
void enqueue_bt_sort(QueueType *q, element item) {
    QueueNode *temp = (QueueNode *)malloc(sizeof(QueueNode));
    if(temp == NULL) {
        error("¸Þ¸ð¸®¸¦ ÇÒ´çÇÒ ¼ö ¾ø½À´Ï´Ù.");
    } else {
        temp->item = item;
        temp->link =NULL;
        
        //  큐가 비어 있으면 원래 방식대로 삽입
        //  큐에 하나가 들어있으면 정렬하지 않아도 됨
        if(is_empty(q)) {
            enqueue(q, item);
        } 
        //  큐에 프로세스가 1개 들어있을 때
        //  즉 새프로세스 - 원래있던프로세스
        //  또는 원래있던프로세스 - 새프로세스
        //  두가지 경우로 나늼
        else if(q->front == q->rear) {
            if(q->front->item.BurTime > item.BurTime) {
                temp->link = q->front;
                q->front = temp;
            } else {
                q->front->link = temp;
                q->rear = temp;
            }
        } 
        //  큐에 2개 이상의 프로세스가 이미 들어있을 때
        else {
            //  큐에서 가장 처음에 삽입되는 경우
            if(q->front->item.BurTime > item.BurTime) {
                temp->link = q->front;
                q->front = temp;
            } 
            //  큐의 마지막에 삽입되는 경우
            else if(q->rear->item.BurTime < item.BurTime) {
                q->rear->link = temp;
                q->rear = temp;
            } 
            //  처음도 마지막도 아닌 중간에 삽입되는 경우
            else {
                QueueNode *t = q->front;
                while(t->link->item.BurTime < item.BurTime) {
                    t = t->link;
                }
                temp->link = t->link;
                t->link = temp;
            }
        }
    }
}

//»èÁ¦ ÇÔ¼ö
element dequeue(QueueType *q){
    QueueNode *temp = q->front;
    element item;
    if(is_empty(q)){					//°ø¹é »óÅÂ
        item.PId = -1;
        return item;
    }else{
        item = temp->item;				//µ¥ÀÌÅÍ¸¦ ²¨³½´Ù.
        q->front = q->front->link;		//front¸¦ ´ÙÀ½ ³ëµå¸¦ °¡¸®Å°µµ·Ï ÇÑ´Ù.
        if(q->front == NULL){			//°ø¹é »óÅÂ
            q->rear=NULL;
        }
        free(temp);						//µ¿Àû ¸Þ¸ð¸® ÇØÁ¦
        return item;					//µ¥ÀÌÅÍ ¹ÝÈ¯
    }
}

double prior_hrn(QueueNode *node, int time) {
    return ((time - node->item.ArrTime) + node->item.BurTime) / (double)node->item.BurTime;
}

//삭제 함수
element dequeue_hrn(QueueType *q, int time){
    QueueNode *temp = q->front;
    element item;
    if(is_empty(q)){					//공백 상태
        item.PId = -1;
        return item;
    }else{
        if(q->front == q->rear) {
            return dequeue(q);
        } else if(q->front->link == q->rear) {
            if(prior_hrn(q->front, time) > prior_hrn(q->rear, time)) {
                temp = q->front;
                q->front = q->rear;
            } else {
                temp = q->rear;
                q->rear = q->front;
            }
            item = temp->item;
            free(temp);
            return item;
        } else {
            double prior = prior_hrn(q->front, time);
            QueueNode *pre_deq = NULL;
            temp = q->front;
            while(temp->link != NULL) {
                if(prior < prior_hrn(temp->link, time)) {
                    prior = prior_hrn(temp->link, time);
                    pre_deq = temp;
                }
                temp = temp->link;
            }
            
            if(pre_deq == NULL) {
                temp = q->front;
                q->front = temp->link;
            } else if(pre_deq->link == q->rear) {
                temp = q->rear;
                q->rear = pre_deq;
                pre_deq->link = NULL;
            } else {
                temp = pre_deq->link;
                pre_deq->link = temp->link;
            }
            item = temp->item;
            free(temp);
            return item;
        }
    }
}

//peekÇÔ¼ö
element peek(QueueType *q){
    if(is_empty(q)){
        element item;
        item.PId = -1;
        return item;
    }else{
        return q->front->item;
    }
}

int selectScheme() {
    char input[10];
    int num;
    
    printf("\n");
    printf("  1. First-Come-First-Service\n");
    printf("  2. Round-Robin\n");
    printf("  3. Shortest-Remaining-Time-Next\n");
    printf("  4. High-Response-Ratio-Next\n");
    printf("  5. Exit\n");
    printf("Select scheduling scheme: ");
    gets(input);
    num=atoi(input);
    fflush(stdin);
    printf("\n");
    
    return num;
}

void printResult(int pnum, int *pid, int *at, int *bt, int *tt, int *wt) {
    int i, sum_tt=0, sum_wt=0;
    
    printf("%15s %15s %15s %15s %15s\n", "Process id", "Arrive time", "Burst time", "Turnaround time", "Waiting time");
    for(i=0; i<pnum; ++i) {
        printf("%15d %15d %15d %15d %15d\n", pid[i], at[i], bt[i], tt[i], wt[i]);
        sum_tt+=tt[i];
        sum_wt+=wt[i];
    }
    
    printf("  avg. of TT: %.2f\n", (float)sum_tt/pnum);
    printf("  avg. of WT: %.2f\n", (float)sum_wt/pnum);
}

void FCFS(int pnum, int *pid, int *at, int *bt, int *tt, int *wt) {
    QueueType q;
    Process p;
    int i,time=0;
    init(&q);
    
    printf("  Scheme: First-Come-First-Servie\n");
    
    for(i=0; i<pnum ; i++){
        p.PId=pid[i];
        p.ArrTime=at[i];
        p.BurTime=bt[i];
        enqueue(&q,p);
    }
    
    
    for(i=0;i<pnum;i++){
        p=dequeue(&q);
        if(time>=p.ArrTime){
            time+=p.BurTime;
        }else{
            time=p.ArrTime+p.BurTime;
        }
        tt[i]=time-p.ArrTime;
        wt[i]=time-p.ArrTime-p.BurTime;
    }
    
}

void RR(int pnum, int *pid, int *at, int *bt, int *tt, int *wt) {
    char input[10];
    int Q;
    int i;
    QueueType q;
    
    Process p;
    Process *cur_process = NULL;    //  ÇöÀç ½ÇÇàÁßÀÎ ÇÁ·Î¼¼½º
    int cur_time = 0;   //  ÇöÀç ½Ã°£
    int tr_count = 0;   //  turn around count - ¿Ï·áµÈ ÇÁ·Î¼¼½º ¼ö
    int remain_Q = 0;   //  ³²Àº Å¸ÀÓÄöÅÒ
    int *insert_time = (int *)malloc(sizeof(int) * pnum);   //  ¸¶Áö¸·À¸·Î »ðÀÔÇÑ ½Ã°£
    
    init(&q);
    
    printf("  Scheme: Round-Robin\n");
    printf("time quantum: ");
    gets(input);
    Q=atoi(input);
    fflush(stdin);
    
    //  Ã¹¹øÂ° ÇÁ·Î¼¼½º »ðÀÔ
    p.PId = pid[0];
    p.ArrTime = at[0];
    p.BurTime = bt[0];
    enqueue(&q, p);
    insert_time[0] = p.ArrTime;
    
    //  ¹Ýº¹¹®ÀÌ µ¹¶§¸¶´Ù ÇöÀç ½Ã°£ÀÌ + 1 µÈ´Ù.
    //  tr_count°¡ pnum°ú °°¾ÆÁö¸é Á¾·á
    while(tr_count != pnum) {
        //  ÇöÀç ¼öÇàÇÒ ÇÁ·Î¼¼½º°¡ ¼³Á¤µÇ¾î ÀÖÁö ¾ÊÀ» ¶§
        if(cur_process == NULL) {
            //  Å¥¿¡ ÇÁ·Î¼¼½º°¡ ÀÖÀ¸¸é ÇöÀç ¼öÇàÇÒ ÇÁ·Î¼¼½º·Î ¼³Á¤
            if(!is_empty(&q)) {
                int pos;
                cur_process = (Process *)malloc(sizeof(Process));
                *cur_process = dequeue(&q);
                remain_Q = Q;   //  Å¸ÀÓÄöÅÒÀ» ÀÔ·Â¹ÞÀº Å¸ÀÓÄöÅÒÀ¸·Î ¼³Á¤
                pos = cur_process->PId - 1;
                wt[pos] += cur_time - insert_time[pos]; //  ´ë±â ½Ã°£ °è»ê
            } else {
                //  Å¥¿¡ ÇÁ·Î¼¼½º°¡ ¾øÀ¸¸é ½Ã°£¸¸ Áõ°¡
                cur_time++;
            }
        }
        
        //  ÇöÀç ÇÁ·Î¼¼½º°¡ ¼³Á¤µÇ¾î ÀÖÀ» ¶§ ÇØ´ç ÇÁ·Î¼¼½º¸¦ ½ÇÇà
        //  Burst Time - 1, ³²Àº Å¸ÀÓ ÄöÅÒ - 1, ÇöÀç½Ã°£ + 1
        if(cur_process != NULL) {
            int pos = cur_process->PId - 1;
            cur_process->BurTime--;
            cur_time++;
            remain_Q--;
            
            //  Burst TimeÀ» ¸ðµÎ ¼ÒÁø ÇÏ¿´À» ¶§
            //  Turn Around TimeÀ» °è»ê
            //  ÇöÀç ÇÁ·Î¼¼½º¸¦ NULL·Î ¼³Á¤ÇÏ¿© ´ÙÀ½ ÇÁ·Î¼¼½º¸¦ °¡Á®¿Àµµ·Ï ÇÔ
            if(cur_process->BurTime == 0) {
                tt[pos] = cur_time - cur_process->ArrTime;
                tr_count++;
                free(cur_process);
                cur_process = NULL;
            } else {
                //  Burst TimeÀÌ ³²¾Æ ÀÖ°í
                //  Å¸ÀÓ ÄöÅÒÀ» ¸ðµÎ ¼ÒÁøÇÏ¿´À» ¶§
                //  Å¥¿¡ ´Ù½Ã ³ÖÀ¸¸é¼­ ´ë±â ½Ã°£À» °è»êÇÏ±â À§ÇØ »ðÀÔ½Ã°£ ±â·Ï
                if(remain_Q == 0) {
                    insert_time[pos] = cur_time;
                    enqueue(&q, *cur_process);
                    free(cur_process);
                    cur_process = NULL;
                }
            }
        }
        
        //  ÇöÀç ½Ã°£°ú Arrived TimeÀÌ ÀÏÄ¡ÇÏ´Â ÇÁ·Î¼¼½º Å¥¿¡ »ðÀÔ
        for(i = 0; i < pnum; i++) {
            if(cur_time == at[i]) {
                p.PId = pid[i];
                p.ArrTime = at[i];
                p.BurTime = bt[i];
                enqueue(&q, p);
                insert_time[i] = cur_time;
            }
        }
    }
}

void SRTN(int pnum, int *pid, int *at, int *bt, int *tt, int *wt) {
    QueueType q;
    Process p;
    int i = 0;
    
    Process *cur_process = NULL;
    int cur_time = 0;
    int tr_count = 0;
    int *insert_time = (int *)malloc(sizeof(int) * pnum);
    
    init(&q);
    
    printf("  Scheme: Shortest-Remaining-Time-Next\n");
    
    p.PId = pid[0];
    p.ArrTime = at[0];
    p.BurTime = bt[0];
    enqueue_bt_sort(&q, p);
    insert_time[0] = p.ArrTime;
 
    while(tr_count != pnum) {
        if(cur_process == NULL) {
            if(!is_empty(&q)) {
                int pos;
                cur_process = (Process *)malloc(sizeof(Process));
                *cur_process = dequeue(&q);
                pos = cur_process->PId - 1;
                wt[pos] += cur_time - insert_time[pos];
            } else {
                cur_time++;
            }
        }
        
        if(cur_process != NULL) {
            int pos = cur_process->PId - 1;
            cur_process->BurTime--;
            cur_time++;
            
            //  Burst TimeÀ» ¸ðµÎ ¼ÒÁøÇÏ¿´À» ¶§
            //  RR°ú ´Þ¸® Å¸ÀÓ ÄöÅÒÀÇ °³³äÀÌ ¾øÀ½
            if(cur_process->BurTime == 0) {
                tt[pos] = cur_time - cur_process->ArrTime;
                tr_count++;
                free(cur_process);
                cur_process = NULL;
            }
        }
        
        for(i = 0; i < pnum; i++) {
            if(cur_time == at[i]) {
                p.PId = pid[i];
                p.ArrTime = at[i];
                p.BurTime = bt[i];
                enqueue_bt_sort(&q, p);
                insert_time[i] = cur_time;
            }
        }
        
        //  Å¥¿¡¼­ ³»¿ëÀ» È®ÀÎÇÏ¿© ÇöÀç ¼öÇàÁßÀÎ ÇÁ·Î¼¼½ºÀÇ Burst Time
        //  º¸´Ù ÀÛÀº ÇÁ·Î¼¼½º°¡ Á¸ÀçÇÏ¸é ÇöÀç ÇÁ·Î¼¼½º¸¦ ¼±Á¡ÇÏ°í Å¥¿¡¼­ °¡Á®¿Â
        //  ÇÁ·Î¼¼½º¸¦ ¼öÇàÁßÀÎ ÇÁ·Î¼¼½º·Î ¼³Á¤
        if(!is_empty(&q) && cur_process != NULL) {
            if(peek(&q).BurTime < cur_process->BurTime) {
                insert_time[cur_process->PId - 1] = cur_time;
                enqueue_bt_sort(&q, *cur_process);
                free(cur_process);
                cur_process = NULL;
            }
        }
    }
}


void HRRN(int pnum, int *pid, int *at, int *bt, int *tt, int *wt) {
    QueueType q;
    Process p;
    int i = 0;
    
    Process *cur_process = NULL;
    int cur_time = 0;
    int tr_count = 0;
    int *insert_time = (int *)malloc(sizeof(int) * pnum);
    
    init(&q);
    printf("  Scheme: High-Response-Ratio-Next\n");
    
    p.PId = pid[0];
    p.ArrTime = at[0];
    p.BurTime = bt[0];
    enqueue_bt_sort(&q, p);
    insert_time[0] = p.ArrTime;
    
    while(tr_count != pnum) {
        if(cur_process == NULL) {
            if(!is_empty(&q)) {
                int pos;
                cur_process = (Process *)malloc(sizeof(Process));
                *cur_process = dequeue_hrn(&q, cur_time);
                pos = cur_process->PId - 1;
                wt[pos] += cur_time - insert_time[pos];
            } else {
                cur_time++;
            }
        }
        
        if(cur_process != NULL) {
            int pos = cur_process->PId - 1;
            cur_process->BurTime--;
            cur_time++;
            
            //  Burst Time을 모두 소진하였을 때
            if(cur_process->BurTime == 0) {
                tt[pos] = cur_time - cur_process->ArrTime;
                tr_count++;
                free(cur_process);
                cur_process = NULL;
            }
        }
        
        for(i = 0; i < pnum; i++) {
            if(cur_time == at[i]) {
                p.PId = pid[i];
                p.ArrTime = at[i];
                p.BurTime = bt[i];
                enqueue_bt_sort(&q, p);
                insert_time[i] = cur_time;
            }
        }
    }
}

int main(void) {
	FILE *fp;
	int pnum, i, exit=0;
	int *pid, *at, *bt, *tt, *wt;

	fp = fopen(INPUT_FILE, "r");
	//read the number of processes 
	fscanf(fp, "%d", &pnum);

	pid = (int*)calloc(pnum, sizeof(int));
	at = (int*)calloc(pnum, sizeof(int));
	bt = (int*)calloc(pnum, sizeof(int));
	tt = (int*)calloc(pnum, sizeof(int));
	wt = (int*)calloc(pnum, sizeof(int));

	for(i=0; i<pnum; ++i) {
		fscanf(fp, "%d %d %d", &pid[i], &at[i], &bt[i]);
	}

	fclose(fp);

	while(!exit) {
		switch(selectScheme()) {
			case 1:
				FCFS(pnum, pid, at, bt, tt, wt);
				break;
			case 2:
				RR(pnum, pid, at, bt, tt, wt);
				break;
			case 3:
				SRTN(pnum, pid, at, bt, tt, wt);
				break;
			case 4: 
				HRRN(pnum, pid, at, bt, tt, wt);
				break;
			case 5:
				exit=1;
				continue;
			default:
				continue;
		}

		printResult(pnum, pid, at, bt, tt, wt);
		memset(tt, 0, sizeof(int)*pnum);
		memset(wt, 0, sizeof(int)*pnum);
	}

	return 0;
}

